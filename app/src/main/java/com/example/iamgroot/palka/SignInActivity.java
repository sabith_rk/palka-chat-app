package com.example.iamgroot.palka;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SignInActivity extends AppCompatActivity {
    Button signInbtn;
    EditText email,pswd;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        signInbtn = (Button) findViewById(R.id.button);
        email = (EditText) findViewById(R.id.field_email);
        pswd = (EditText) findViewById(R.id.field_password);

        signInbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(SignInActivity.this,MainActivity.class);
                String email_str = email.getText().toString();
                String pswd_str = pswd.getText().toString();
                intent.putExtra("email",email_str);
                intent.putExtra("password",pswd_str);
                startActivity(intent);
            }
        });
    }
}
